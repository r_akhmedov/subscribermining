package com.azercell.subscribermining.data;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

/**
 *
 * @author itrahim
 */
public class CCNData implements Serializable {


    //public final String serviceContextID;     //0 not needed will be used to detect roaming SMS
    //public final String serviceSessionID;     //1
    public final String servedMSISDN;         //2
    public final String sessionId;            //3 in case data => chargingid; case serviceContextID=CAPv2 => servicesessionid; case serviceContextID=SCAP => sessionid

    public final String serviceId;  //4 //voice sms data mms diameter
    public final long timeUnit;             //5
    public final long totalOctetsUnit;      //6
    public final long serviceSpecificUnit;  //7

    public final long eventTime;          //8 charging timestamp in long format

    public final String serviceScenario;    //9
    public final String roamingPosition;    //10 -- decode for SMS

    //ccTariffInfo [11]  IA5STRING OPTIONAL, -- convert to IA5String SEQUENCE OF SelectionTreeParameter //11

    public final String serviceClassID;     //12
    public final String fafID; //15

    public final String accountID; // MAIN, DA_x
    public final double accountValueBefore; //13 if main account
    public final double accountValueAfter;  //14 if main account
    public final double accountValueDeducted;   //16 if main account
    public final long unitsBefore, unitsAfter, unitsChange;

    //public final String dedicatedAccounts;    //17
    /*
    --ccDedicatedAccount1ID          [110] DedicatedAccountID OPTIONAL,
    --ccDedicatedAccount1ValueBefore [111] IA5STRING OPTIONAL, -- convert to IA5String MonetaryUnits
    --ccDedicatedAccount1ValueAfter  [112] IA5STRING OPTIONAL, -- convert to IA5String MonetaryUnits
    --ccDedicatedAccount1Change      [113] IA5STRING OPTIONAL, -- convert to IA5String MonetaryUnits
    --ccDA1UnitsBefore 		[160] INTEGER OPTIONAL,
    --ccDA1UnitsAfter  		[161] INTEGER OPTIONAL,
    --ccDA1UnitsChange      	[162] INTEGER OPTIONAL,
    */
    public final String communityID;    //18
    //public final String chargingid;           //19
    public final String apn;                  //20
    public final String lai;               //21
    public final String ratingGroup;          //22
    public final String locno;                //23
    public final String opnumber;             //24 populate only diamter numbers
    public final String zoneTDF;              //25
    public final String plmnID;               //26
    public final String imei;                 //27 only first 8 digits

    private CCNData(String msisdn, String sessionid, String serviceid,
                    long timeu, long bytes, long eventu, long timeid,
                    String servsenario, String roampos, String sc, String faf,
                    String accid, double valB, double valA, double charge,
                    long unitb, long unita, long unitc, String commid, String apn,
                    String lai, String rg, String locno, String opnum, String zone,
                    String plmn, String imei)
    {
        this.servedMSISDN = msisdn;
        this.sessionId = sessionid;
        this.serviceId = serviceid;
        this.timeUnit = timeu;
        this.totalOctetsUnit = bytes;
        this.serviceSpecificUnit = eventu;
        this.eventTime = timeid;
        this.serviceScenario = servsenario;
        this.roamingPosition = roampos;
        this.serviceClassID = sc;
        this.fafID = faf;
        this.accountID = accid;
        this.accountValueBefore = valB;
        this.accountValueAfter = valA;
        this.accountValueDeducted = charge;
        this.unitsBefore = unitb;
        this.unitsAfter = unita;
        this.unitsChange = unitc;
        this.communityID = commid;
        this.apn = apn;
        this.lai = lai;
        this.ratingGroup = rg;
        this.locno = locno;
        this.opnumber = opnum;
        this.zoneTDF = zone;
        this.plmnID = plmn;
        this.imei = imei;
    }

    public static ArrayList<CCNData> getRecords(String record) throws ParseException
    {
        ArrayList<CCNData> retval = new ArrayList<CCNData>();
        String [] fields = record.split("\\,", 33);
        String [] deas = fields[17].split("\\|");


        String sessionid;
        if(fields[0].startsWith("Ericsson_OCS"))
            sessionid = fields[19];
        else if (fields[0].startsWith("SCAP"))
            sessionid = fields[3];
        else sessionid = fields[1];

        String service;
        if (fields[4] == null || fields[4].isEmpty())
            service = "none";
        else if(Integer.parseInt(fields[4]) == 4)
            service = "sms";
        else if(Integer.parseInt(fields[4]) == 3)
            service = "diameter";
        else if(Integer.parseInt(fields[4]) == 6)
            service = "mms";
        else if(Integer.parseInt(fields[4]) >= 1000)
            service = fields[4];
        else service = "voice";

        String roampos;
        if(service.equals("sms") && fields[0].startsWith("SCAP"))
            roampos = "r";
        else if(service.equals("data") && !fields[26].equals("40001") && fields[26].length() >=5)
            roampos = "r";
        else roampos = fields[10];

        long eventtime = (fields[8] == null || fields[8].isEmpty()) ? 0 : new SimpleDateFormat("yyyyMMddHHmmss").parse(fields[8].substring(0, 14)).getTime();

        double deacharge = 0.0;
        for(String dea: deas) {
            if (dea == null || dea.isEmpty() )
                continue;
            String [] deafields = dea.split("\\:", 7);
            if (deafields[3] != null && !deafields[3].isEmpty())
                deacharge += Double.parseDouble(deafields[3]);
            retval.add(new CCNData(fields[2],
                    sessionid,
                    service,
                    0, 0, 0,
                    eventtime,
                    fields[9],
                    roampos,
                    fields[12],
                    fields[15],
                    ("DA_"+deafields[0]),
                    deafields[1] != null && !deafields[1].isEmpty() ? Double.parseDouble(deafields[1]): 0.0,
                    deafields[2] != null && !deafields[2].isEmpty() ? Double.parseDouble(deafields[2]): 0.0,
                    deafields[3] != null && !deafields[3].isEmpty() ? Double.parseDouble(deafields[3]): 0.0,
                    deafields[4] != null && !deafields[4].isEmpty() ? Long.parseLong(deafields[4]): 0,
                    deafields[5] != null && !deafields[5].isEmpty() ? Long.parseLong(deafields[5]): 0,
                    deafields[6] != null && !deafields[6].isEmpty() ? Long.parseLong(deafields[6]): 0,
                    fields[18],
                    fields[20],
                    fields[21],
                    fields[22],
                    (roampos.equals("r") && fields[23] != null && fields[23].length() > 5 ? fields[23].substring(0, 5) : fields[23]),
                    (service.equals("diameter") ? fields[24].substring(2) : ""),
                    fields[25],
                    fields[26],
                    fields[27] != null && fields[27].length() > 8 ? fields[27].substring(0, 8) : ""
            ));
        }

        double mbcharge =0.0;
        if (fields[16].length()>0)
            mbcharge =
                    (fields[16] != null && !fields[16].isEmpty() ? Double.parseDouble(fields[16]): 0.0) - deacharge;


        retval.add(new CCNData(
                fields[2],
                sessionid,
                service,
                fields[5] != null && !fields[5].isEmpty() ? Long.parseLong(fields[5]): 0,
                fields[6] != null && !fields[6].isEmpty() ? Long.parseLong(fields[6]): 0,
                fields[7] != null && !fields[7].isEmpty() ? Long.parseLong(fields[7]): 0,
                eventtime,
                fields[9],
                roampos,
                fields[12],
                fields[15],
                "MAIN",
                fields[13] != null && !fields[13].isEmpty() ? Double.parseDouble(fields[13]) : 0.0,
                fields[14] != null && !fields[14].isEmpty() ? Double.parseDouble(fields[14]) : 0.0,
                mbcharge,
                0, 0, 0,
                fields[18],
                fields[20],
                fields[21],
                fields[22],
                (roampos.equals("r") && fields[23] != null && fields[23].length() > 5 ? fields[23].substring(0, 5) : fields[23]),
                (service.equals("diameter") ? fields[24].substring(2) : ""),
                fields[25],
                fields[26],
                fields[27] != null && fields[27].length() > 8 ? fields[27].substring(0, 8) : "" ));

        return retval;
    }

    public String getDataKey(long timeRound){

        return String.format("%d/%s/%s/%s/%s/%s",
                this.eventTime - (this.eventTime % timeRound),
                this.servedMSISDN,
                this.serviceId,
                this.accountID.startsWith("DA") ? "DA" : this.accountID, // grouping MAIN account and all Dedicated accounts
                this.apn,
                this.ratingGroup);
    }

    public static CCNData sumupData(CCNData data1, CCNData data2, long timeRound) throws KeyMismatchException {
        if (data1 != null && data2 == null) return data1;
        if (data2 != null && data1 == null) return data2;
        if (!data1.getDataKey(timeRound).
                equals(data2.getDataKey(timeRound)))
            throw new KeyMismatchException();
        return data1.eventTime >= data2.eventTime ? new CCNData(
                data1.servedMSISDN,
                null,   //sessionId,
                data1.serviceId,
                0,      //data1.timeUnit+data2.timeUnit,
                data1.totalOctetsUnit+data2.totalOctetsUnit,
                0,      //data1.serviceSpecificUnit+data2.serviceSpecificUnit,
                data1.eventTime,
                null,   //data1.serviceScenario,
                null,   //data1.roamingPosition,
                data1.serviceClassID,
                null,   //data1.fafID,
                null,   //data1.accountID,
                0.0,    //data1.accountValueBefore,
                0.0,    //data1.accountValueAfter,
                data1.accountValueDeducted + data2.accountValueDeducted,
                0,      //data1.unitsBefore,
                0,      //data1.unitsAfter,
                data1.unitsChange + data2.unitsChange,
                null,   //data1.communityID,
                data1.apn,
                null,   //data1.lai,
                data1.ratingGroup,
                null,   //data1.locno,
                null,   //data1.opnumber,
                null,   //data1.zoneTDF,
                null,   //data1.plmnID,
                null    //data1.imei
        ) : new CCNData(
                data2.servedMSISDN,
                null,   //data2.sessionId,
                data2.serviceId,
                0,      //data1.timeUnit+data2.timeUnit,
                data1.totalOctetsUnit+data2.totalOctetsUnit,
                0,      //data1.serviceSpecificUnit+data2.serviceSpecificUnit,
                data2.eventTime,
                null,   //data2.serviceScenario,
                null,   //data2.roamingPosition,
                data2.serviceClassID,
                null,   //data2.fafID,
                null,   //data2.accountID,
                0.0,    //data2.accountValueBefore,
                0.0,    //data2.accountValueAfter,
                data1.accountValueDeducted + data2.accountValueDeducted,
                0,      //data2.unitsBefore,
                0,      //data2.unitsAfter,
                data1.unitsChange + data2.unitsChange,
                null,   //data2.communityID,
                data2.apn,
                null,   //data2.lai,
                data2.ratingGroup,
                null,   //data2.locno,
                null,   //data2.opnumber,
                null,   //data2.zoneTDF,
                null,   //data2.plmnID,
                null    //data2.imei
        );
    }



}
