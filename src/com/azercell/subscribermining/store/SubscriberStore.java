package com.azercell.subscribermining.store;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.HColumnDescriptor;
import org.apache.hadoop.hbase.HTableDescriptor;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.*;
import org.apache.hadoop.hbase.io.compress.Compression;
import org.apache.hadoop.hbase.util.Bytes;

import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by rahimahmedov on 11/21/15.
 */
public class SubscriberStore {

    private Table mTable;
    private ArrayList<Put> mPuts;
    private ArrayList<Delete> mDeletes;
    private Configuration mConfig;
    private Connection mConnection;

    public static void init(String zkQuorum, int zkPort, String tableName, String colFamName) throws IOException {

        // initialize HBase config...
        Configuration config = HBaseConfiguration.create();
        config.set("hbase.zookeeper.quorum", zkQuorum);
        config.setInt("hbase.zookeeper.property.clientPort", zkPort);
        Connection conn = ConnectionFactory.createConnection(config);
        Admin hadmin = conn.getAdmin();
        HTableDescriptor tableDescriptor;
        HColumnDescriptor colDescriptor;

        if (!hadmin.tableExists(TableName.valueOf(tableName)))
        {
            tableDescriptor = new HTableDescriptor(TableName.valueOf(tableName));
            colDescriptor = new HColumnDescriptor(colFamName);
            colDescriptor.setCompressionType(Compression.Algorithm.SNAPPY);
            tableDescriptor.addFamily(colDescriptor);
            hadmin.createTable(tableDescriptor);
        }
        else
        {
            tableDescriptor = hadmin.getTableDescriptor(TableName.valueOf(tableName));
            if (! tableDescriptor.hasFamily(Bytes.toBytes(colFamName))) {
                colDescriptor = new HColumnDescriptor(colFamName);
                colDescriptor.setCompressionType(Compression.Algorithm.SNAPPY);
                tableDescriptor.addFamily(colDescriptor);
                hadmin.modifyTable(TableName.valueOf(tableName), tableDescriptor);
            }
        }
        conn.close();
    }

    public SubscriberStore(String zkQuorum, int zkPort, String tableName) throws IOException {

        //TODO: initialize storage
        this.mConfig = HBaseConfiguration.create();
        this.mConfig.set("hbase.zookeeper.quorum", zkQuorum);
        this.mConfig.setInt("hbase.zookeeper.property.clientPort", zkPort);
        this.mConnection = ConnectionFactory.createConnection(this.mConfig);
        this.mTable  = this.mConnection.getTable(TableName.valueOf(tableName));
        this.mPuts = new ArrayList<>();
        this.mDeletes = new ArrayList<>();
        this.mTable.setWriteBufferSize(128*1024*1024);
        //this.mTable.setAutoFlushTo(false);
    }

    public void addPut(Put put) {
        this.mPuts.add(put);
    }

    public void addDelete(Delete delete) {
        this.mDeletes.add(delete);
    }

    public Result get(Get get) throws IOException {
        //get.setCheckExistenceOnly(false);
        return this.mTable.get(get);
    }

    public void commitAndFinalize() throws IOException {
        if (this.mPuts.size() > 0) this.mTable.put(this.mPuts);
        if (this.mDeletes.size() > 0) this.mTable.delete(this.mDeletes);
        //this.mTable.flushCommits();
        this.mTable.close();
        this.mConnection.close();
    }



}
