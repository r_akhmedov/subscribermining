package com.azercell.subscribermining.store;


import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.*;
import org.apache.hadoop.hbase.filter.*;
import org.apache.hadoop.hbase.mapreduce.TableInputFormat;
import org.apache.hadoop.hbase.protobuf.ProtobufUtil;
import org.apache.hadoop.hbase.util.Base64;
import org.apache.hadoop.hbase.util.Bytes;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;

/**
 * Created by rahimahmedov on 12/1/15.
 */
public class SubscriberQuery {

    private static final long DAY = 24*3600*1000;

    private Configuration mHConfig;
    private Table mTable;
    private Connection mConnection;
    private FilterList mFilterList;
    //private Scan mScan;

    private void addFilter(Filter filter) {
        if (this.mFilterList == null) {
            this.mFilterList = new FilterList(FilterList.Operator.MUST_PASS_ALL);
        }
        this.mFilterList.addFilter(filter);
    };

    public SubscriberQuery(String zkQuorum, int zkPort) throws IOException {
        this.mHConfig = HBaseConfiguration.create();
        this.mHConfig.set("hbase.zookeeper.quorum", zkQuorum);
        this.mHConfig.setInt("hbase.zookeeper.property.clientPort", zkPort);
        this.mConnection = ConnectionFactory.createConnection(this.mHConfig);
        //this.mScan = new Scan();
    }

    public SubscriberQuery from(String table) throws IOException {
        this.mTable = this.mConnection.getTable(TableName.valueOf(table));
        this.mHConfig.set(TableInputFormat.INPUT_TABLE, table);
        return this;
    }



    /*
    queryStr format => {msisdn/date}
            msisdn => {* | rng1-rng2 |m1,m2..,m }
            date => { * | rng1-rng2 | date1,date2,... }

     */
    public SubscriberQuery rows(String queryStr) throws ParseException {

        String msisdnList = queryStr.split("/")[0];
        String dateList = queryStr.split("/")[1];

        if (msisdnList.equals("*") && dateList.equals("*")) return this;

        if(msisdnList.contains("-")) {
            this.addFilter(new RowFilter(CompareFilter.CompareOp.GREATER_OR_EQUAL,
                    new BinaryPrefixComparator(Bytes.toBytes(msisdnList.split("-")[0]))));
            this.addFilter(new RowFilter(CompareFilter.CompareOp.LESS_OR_EQUAL,
                    new BinaryPrefixComparator(Bytes.toBytes(msisdnList.split("-")[0]))));
        }
        else if(msisdnList.contains(",")) {
            for (String msisdn: msisdnList.split(","))
            {
                this.addFilter(new RowFilter(CompareFilter.CompareOp.EQUAL,
                        new BinaryPrefixComparator(Bytes.toBytes(msisdn))));
            }
        }
        else {
            this.addFilter(new RowFilter(CompareFilter.CompareOp.EQUAL,
                    new BinaryPrefixComparator(Bytes.toBytes(msisdnList))));
        }

        if(dateList.contains("-")) {
            long startdt = new SimpleDateFormat("yyyymmdd").parse(dateList.split("-")[0]).getTime();
            long enddt = new SimpleDateFormat("yyyymmdd").parse(dateList.split("-")[1]).getTime();
            while (enddt > startdt) {
                this.addFilter(new RowFilter(CompareFilter.CompareOp.EQUAL,
                        new SubstringComparator(String.format("/%d", startdt))));
                startdt += DAY;
            }
        }
        else if(dateList.contains(",")) {
            for (String date: dateList.split(",")) {
               this.addFilter(new RowFilter(CompareFilter.CompareOp.EQUAL,
                       new SubstringComparator(String.format("/%d", new SimpleDateFormat("yyyymmdd").parse(date).getTime()))));
            }
        }
        else if (dateList.matches("^[0-9]{8}$")){
            this.addFilter(new RowFilter(CompareFilter.CompareOp.EQUAL,
                    new SubstringComparator(String.format("/%d", new SimpleDateFormat("yyyymmdd").parse(dateList).getTime()))));
        }

        return this;
    }

    /*
    queryString => colfam:qualifier1,..
     */
    public SubscriberQuery columns(String queryString) {
        for (String colfamQualifier: queryString.split(",")) {
            this.addFilter(new FamilyFilter(CompareFilter.CompareOp.EQUAL,
                    new BinaryComparator(Bytes.toBytes(colfamQualifier.split(":")[0]))));
            this.addFilter(new QualifierFilter(CompareFilter.CompareOp.EQUAL,
                    new SubstringComparator(colfamQualifier.split(":")[1])));
        }
        return this;
    }

    public SubscriberResult scan(boolean groupByMsisdn) throws IOException {
        SubscriberResult sr = new SubscriberResult(groupByMsisdn);
        Scan scan = new Scan();
        if (this.mFilterList != null) scan.setFilter(this.mFilterList);
        ResultScanner rscan = this.mTable.getScanner(scan);
        for (Result res: rscan)
            sr.add(res);
        return sr;
    }


    public FilterList getFilterList() {
        return this.mFilterList;
    }

    public Configuration getAsConfig() throws IOException {
        Scan scan = new Scan();
        if (this.mFilterList != null)
            scan.setFilter(this.mFilterList);
        this.mHConfig.set(TableInputFormat.SCAN, Base64.encodeBytes(ProtobufUtil.toScan(scan).toByteArray()));
        this.mHConfig.setBoolean("hbase.defaults.for.version.skip", true);
        return this.mHConfig;
    }

}
