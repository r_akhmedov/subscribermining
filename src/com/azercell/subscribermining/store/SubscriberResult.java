package com.azercell.subscribermining.store;

import org.apache.hadoop.hbase.Cell;
import org.apache.hadoop.hbase.client.Result;
import org.apache.hadoop.hbase.filter.FilterList;
import org.apache.hadoop.hbase.util.Bytes;
import org.apache.spark.SparkContext;
import org.apache.spark.api.java.JavaPairRDD;
import scala.Tuple2;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.TreeSet;

/**
 * Created by rahimahmedov on 12/5/15.
 */
public class SubscriberResult {

    //private ArrayList<SubscriberModel> mSubscribers;

    //MSISDN->{DATE->{columnName: Value} }
    private HashMap<String, HashMap<Long, HashMap<String, Double>>> mSubscriberList;
    private TreeSet<String> mColumnList;
    private boolean mGroupByMsisdn = true;


    public SubscriberResult() {
        this.mColumnList = new TreeSet<>();
        this.mSubscriberList = new HashMap<>();
    }

    public SubscriberResult(boolean groupByMsisdn) {
        this.mColumnList = new TreeSet<>();
        this.mSubscriberList = new HashMap<>();
        this.mGroupByMsisdn = groupByMsisdn;
    }

    public void add(Result res) {
        String msisdn = Bytes.toString(res.getRow()).split("/")[0];
        long date = this.mGroupByMsisdn ? 0 : Long.parseLong(Bytes.toString(res.getRow()).split("/")[1]);
        if (!this.mSubscriberList.containsKey(msisdn))
            this.mSubscriberList.put(msisdn, new HashMap<Long, HashMap<String, Double>>());
        if (!this.mSubscriberList.get(msisdn).containsKey(date))
            this.mSubscriberList.get(msisdn).put(date, new HashMap<String, Double>());
        for (Cell cell: res.listCells()) {
            String fmCol = Bytes.toString(cell.getFamilyArray(), cell.getFamilyOffset(), cell.getFamilyLength()) + ":" +
                    Bytes.toString(cell.getQualifierArray(), cell.getQualifierOffset(), cell.getQualifierLength());
            double val = Bytes.toDouble(cell.getValueArray(), cell.getValueOffset());
            mColumnList.add(fmCol);
            if (!this.mSubscriberList.get(msisdn).get(date).containsKey(fmCol))
                this.mSubscriberList.get(msisdn).get(date).put(fmCol, val);
            else
                this.mSubscriberList.get(msisdn).get(date).put(fmCol, val+this.mSubscriberList.get(msisdn).get(date).get(fmCol));
        }
    }


    public TreeSet<String> getColumns(){
        return this.mColumnList;
    }

    public HashMap<String, HashMap<Long, HashMap<String, Double>>> getSubscriberList() {
        return this.mSubscriberList;
    }

    public ArrayList<Tuple2<String, ArrayList<Double>>> getAsList() {
        ArrayList<Tuple2<String, ArrayList<Double>>> list = new ArrayList<>();
        for(String msisdn: this.mSubscriberList.keySet()) {
            HashMap<Long, HashMap<String, Double>> dateColVal = this.mSubscriberList.get(msisdn);
            for (long date: dateColVal.keySet()) {
                ArrayList<Double> vector = new ArrayList<>();
                int idx = 0;
                vector.add(idx, (double)date);
                for (String fmcol : this.mColumnList) {
                    HashMap<String, Double> colVal = this.mSubscriberList.get(msisdn).get(date);
                    vector.add(++idx, colVal.containsKey(fmcol) ? colVal.get(fmcol) : 0);
                }
                list.add(new Tuple2(msisdn, vector));
            }
        }
        return list;
    }

    public Tuple2<String, Double[]> [] getAsList2() {
        Tuple2 [] list = new Tuple2[this.mSubscriberList.keySet().size()];
        int i =0;
        for(String msisdn: this.mSubscriberList.keySet()) {
            HashMap<Long, HashMap<String, Double>> dateColVal = this.mSubscriberList.get(msisdn);
            for (long date: dateColVal.keySet()) {
                Double [] vector = new Double[this.mColumnList.size()+1];
                int idx = 0;
                vector[idx] = (double)date;
                for (String fmcol : this.mColumnList) {
                    HashMap<String, Double> colVal = this.mSubscriberList.get(msisdn).get(date);
                    vector[++idx] = colVal.containsKey(fmcol) ? colVal.get(fmcol) : 0;
                }
                list[i++] = new Tuple2(msisdn, vector);
            }
        }
        return list;
    }



}
