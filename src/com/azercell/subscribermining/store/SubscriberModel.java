package com.azercell.subscribermining.store;

import com.azercell.subscribermining.data.CCNData;
import com.azercell.subscribermining.data.KeyMismatchException;
import org.apache.hadoop.hbase.Cell;
import org.apache.hadoop.hbase.KeyValue;
import org.apache.hadoop.hbase.client.Get;
import org.apache.hadoop.hbase.client.Put;
import org.apache.hadoop.hbase.client.Result;
import org.apache.hadoop.hbase.util.Bytes;
import org.apache.hadoop.yarn.webapp.hamlet.HamletSpec;
import scala.Tuple2;

import java.io.Serializable;
import java.util.HashMap;

/**
 * Created by rahimahmedov on 11/21/15.
 */
public class SubscriberModel implements Serializable {

    public final String accountNumber;
    public final long eventTime;
    public final String domain;
    public final HashMap<String, Double> paramValue;

    private SubscriberModel(String domain, String accountNumber, long eventTime, HashMap<String, Double> paramValue) {
        this.accountNumber = accountNumber;
        this.domain = domain;
        this.eventTime = eventTime;
        this.paramValue = paramValue;
    }

    public static SubscriberModel create(String domain, String accountNumber, long eventTime, HashMap<String, Double> paramValue) {
        return new SubscriberModel(domain,accountNumber,eventTime,paramValue);
    }

    public static SubscriberModel create(String domain, long timeRound, CCNData ccndata) {
        HashMap<String, Double> paramValue = new HashMap<String, Double>();
        paramValue.put(String.format("%s/%s/%s/%s", ccndata.ratingGroup, ccndata.serviceId, ccndata.apn, "MB"),ccndata.totalOctetsUnit/1024.0/1024.0);
        if (ccndata.accountID.startsWith("DA")) {
            paramValue.put("DA_MONTEARY_CHARGE", ccndata.accountValueDeducted);
            paramValue.put("DA_UNITS_CHARGE", (double)ccndata.unitsChange);
        }
        if (ccndata.accountID.equals("MAIN"))
            paramValue.put("MAIN_BALANCE_CHARGE", ccndata.accountValueDeducted);
        return SubscriberModel.create(domain, ccndata.servedMSISDN, ccndata.eventTime - (ccndata.eventTime % timeRound), paramValue);
    }


    public static SubscriberModel sumup(SubscriberModel sm1, SubscriberModel sm2) throws KeyMismatchException {
        if (! (sm1.accountNumber.equals(sm2.accountNumber) &&
                sm1.domain.equals(sm2.domain) &&
                sm1.eventTime == sm2.eventTime) )
            throw new KeyMismatchException();

        HashMap<String, Double> pv = sm1.paramValue;

        for (String key: sm2.paramValue.keySet()) {
            if(pv.containsKey(key))
                pv.put(key, pv.get(key)+sm2.paramValue.get(key));
            else pv.put(key, sm2.paramValue.get(key));
        }

        return  new SubscriberModel(sm1.domain, sm1.accountNumber, sm1.eventTime, pv);
    }

    public Put getHbasePut() {
        Put put = new Put(Bytes.toBytes(this.accountNumber +"/"+ String.valueOf(this.eventTime)));
        for (String key: this.paramValue.keySet())
            put.addColumn(Bytes.toBytes(this.domain), Bytes.toBytes(key), Bytes.toBytes(this.paramValue.get(key)));
        return put;
    }

    public Get getHbaseGet() {
        return new Get(Bytes.toBytes(this.accountNumber +"/"+ String.valueOf(this.eventTime)));
    }

    public static SubscriberModel convertHbaseRes2Model(Result res) {
        if (res.isEmpty())
            return null;
        HashMap<String, Double> paramValue = new HashMap<>();
        String [] row = Bytes.toString(res.getRow()).split("/");
        String domain = Bytes.toString(res.rawCells()[0].getFamilyArray(), res.rawCells()[0].getFamilyOffset(), res.rawCells()[0].getFamilyLength());
        for (Cell cell: res.rawCells())
            paramValue.put( Bytes.toString(cell.getQualifierArray(), cell.getQualifierOffset(), cell.getQualifierLength()),
                            Bytes.toDouble(cell.getValueArray(), cell.getValueOffset()));
        return new SubscriberModel(domain, row[0], Long.parseLong(row[1]), paramValue);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("{ ").append(String.format("domain: %s, msisdn: %s, eventtime: %d, paramValue: {", this.domain, this.accountNumber, this.eventTime));
        for (String key: this.paramValue.keySet())
            sb.append(String.format("%s: %f, ", key, this.paramValue.get(key)));
        sb.append("} }");
        return sb.toString();
    }


}
