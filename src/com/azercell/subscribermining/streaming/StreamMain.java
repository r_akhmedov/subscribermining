package com.azercell.subscribermining.streaming;

import com.azercell.subscribermining.data.CCNData;
import com.azercell.subscribermining.store.SubscriberModel;
import com.azercell.subscribermining.store.SubscriberStore;
import org.apache.hadoop.hbase.client.*;
import org.apache.spark.streaming.api.java.JavaStreamingContext;

import java.io.IOException;
import java.net.URI;
import java.util.Date;
import java.util.Iterator;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.function.FlatMapFunction;
import org.apache.spark.api.java.function.Function;
import org.apache.spark.api.java.function.Function2;
import org.apache.spark.api.java.function.PairFunction;
import org.apache.spark.api.java.function.VoidFunction;
import org.apache.spark.streaming.Durations;
import org.apache.spark.streaming.api.java.JavaDStream;
import org.apache.spark.streaming.api.java.JavaStreamingContextFactory;
import org.apache.spark.streaming.flume.FlumeUtils;
import org.apache.spark.streaming.flume.SparkFlumeEvent;
import scala.Tuple2;
import org.apache.hadoop.hbase.util.Bytes;


/**
 * Created by rahimahmedov on 11/18/15.
 */
public class StreamMain {

    public static final int DAY = 24*3600*1000;
    public static final String DOMAIN = "DATA";

    public static JavaStreamingContext createContext(String confFile) throws IOException {

        /*
        Streaming configuration:
           com.azercell.subscribermining.stream.flume.port    -- flume streaming port
           com.azercell.subscribermining.stream.flume.host    -- flume streaming host
           com.azercell.subscribermining.stream.interval      -- streaming batch interval
           com.azercell.subscribermining.stream.checkpoint    -- streaming check point
           com.azercell.subscribermining.retention            -- storage (hbase) retention in days
           com.azercell.subscribermining.hbase.zk.port        -- hbase zookeeper port
           com.azercell.subscribermining.hbase.zk.quorum      -- hbase zookeeper quorum
           com.azercell.subscribermining.hbase.table          -- hbase zookeeper quorum
        */

        Configuration conf = new Configuration();
        FileSystem fs = FileSystem.newInstance(conf);
        conf.addResource(fs.open(new Path(URI.create(confFile).getPath())));
        conf.reloadConfiguration();

        final int retention     = conf.getInt("com.azercell.subscribermining.retention", 1)*DAY;
        final int batchDur      = conf.getInt("com.azercell.subscribermining.stream.interval", -1);
        final int ccnDur        = DAY;
        final int port          = conf.getInt("com.azercell.subscribermining.stream.flume.port", -1);
        final String host       = conf.get("com.azercell.subscribermining.stream.flume.host");
        final String chkpoint   = conf.get("com.azercell.subscribermining.stream.checkpoint");
        final String zkQuorum   = conf.get("com.azercell.subscribermining.hbase.zk.quorum");
        final String hTableName = conf.get("com.azercell.subscribermining.hbase.table");
        final int zkPort        = conf.getInt("com.azercell.subscribermining.hbase.zk.port", 2181);

        SparkConf sparkConf = new SparkConf();
        sparkConf.setAppName("SubscriberMining");

        System.out.println(String.format("checkPoint: %s\nbatchdur: %d\nccndur: %d\nretention: %d\nflumeHost: %s\nflumePort: %d\nzkQuorum: %s\nhTable: %s\n",
                chkpoint, batchDur, ccnDur, retention, host, port, zkQuorum, hTableName));

        JavaStreamingContext jssc = new JavaStreamingContext(sparkConf, Durations.seconds(batchDur));
        jssc.checkpoint(chkpoint);

        JavaDStream<SparkFlumeEvent> flumeEvent = FlumeUtils.createPollingStream(jssc, host, port);

        flumeEvent.
                flatMap(new FlatMapFunction<SparkFlumeEvent, CCNData>() {
                    public Iterable<CCNData> call(SparkFlumeEvent sparkFlumeEvent) throws Exception {
                        return CCNData.getRecords(new String(sparkFlumeEvent.event().getBody().array()));
                    }
                }).
                filter(new Function<CCNData, Boolean>() {
                    public Boolean call(CCNData v1) throws Exception {
                        return v1.servedMSISDN != null &&
                                v1.serviceId != null &&
                                v1.apn != null &&
                                v1.ratingGroup != null &
                                v1.serviceId.matches("[0-9]+") &&
                                (new Date().getTime() - v1.eventTime < retention);
                    }
                }).
                mapToPair(new PairFunction<CCNData, String, SubscriberModel>() {
                    public Tuple2<String, SubscriberModel> call(CCNData t) {
                        return new Tuple2<String, SubscriberModel>(
                                String.format("%s/%s/%d",
                                    DOMAIN,
                                    t.servedMSISDN,
                                    t.eventTime - (t.eventTime % ccnDur)),
                                SubscriberModel.create(DOMAIN, ccnDur, t));
                    }
                }).
                reduceByKey(new Function2<SubscriberModel, SubscriberModel, SubscriberModel>() {
                    @Override
                    public SubscriberModel call(SubscriberModel subscriberModel, SubscriberModel subscriberModel2) throws Exception {
                        return SubscriberModel.sumup(subscriberModel, subscriberModel2);
                    }
                }).
                foreachRDD(new Function<JavaPairRDD<String, SubscriberModel>, Void>() {
                    public Void call(JavaPairRDD<String, SubscriberModel> t1) throws Exception {
                        t1.foreachPartitionAsync(new VoidFunction<Iterator<Tuple2<String, SubscriberModel>>>(){
                            public void call(Iterator<Tuple2<String, SubscriberModel>> t) throws Exception {

                                SubscriberStore ss = new SubscriberStore(zkQuorum, zkPort, hTableName);

                                while(t.hasNext()) {
                                    Tuple2<String, SubscriberModel> tuple = t.next();
                                    Result res = ss.get(tuple._2().getHbaseGet());
                                    if (!res.isEmpty()) {
                                        SubscriberModel sm = SubscriberModel.convertHbaseRes2Model(res);
                                        ss.addPut(SubscriberModel.sumup(tuple._2(), sm).getHbasePut());
                                    }
                                    else ss.addPut(tuple._2().getHbasePut());
                                    ss.addDelete(new Delete(Bytes.toBytes(String.format("%s/%d", tuple._2().accountNumber, tuple._2().eventTime - retention))));
                                }
                                ss.commitAndFinalize();
                            }
                        });
                        return null;
                    }
                });

        return jssc;
    }

    public static void main(String [] args) throws IOException {
        final String confFile = args[0];

        Configuration conf = new Configuration();
        FileSystem fs = FileSystem.newInstance(conf);
        conf.addResource(fs.open(new Path(URI.create(confFile).getPath())));
        conf.reloadConfiguration();

        SubscriberStore.init(
                conf.get("com.azercell.subscribermining.hbase.zk.quorum"),
                conf.getInt("com.azercell.subscribermining.hbase.zk.port", 2181),
                conf.get("com.azercell.subscribermining.hbase.table"),
                DOMAIN);

        final String chkpointdir = conf.get("com.azercell.subscribermining.stream.checkpoint");

        JavaStreamingContextFactory jscf = new JavaStreamingContextFactory() {
            @Override
            public JavaStreamingContext create() {
                try {
                    return createContext(confFile);
                }  catch (IOException ex) {
                    ex.printStackTrace();
                    return null;
                }
            }
        };
        JavaStreamingContext jssc = JavaStreamingContext.getOrCreate(chkpointdir, jscf);
        jssc.start();
        jssc.awaitTermination();
    }
}
