package sm

import org.apache.hadoop.hbase.client.Result
import org.apache.hadoop.hbase.util.Bytes
import org.apache.spark.mllib.linalg.Vectors
import scala.collection.JavaConversions._

/**
  * Created by itrahim on 12/23/2015.
  */
case class SubsModelData(
  msisdn: String,
  free_rg: Double,
  payasgo_rg: Double,
  pack_rg: Double,
  other_rg: Double)
{
  def +(sm: SubsModelData) =
        new SubsModelData(this.msisdn, this.free_rg+sm.free_rg, this.payasgo_rg+sm.payasgo_rg, this.pack_rg+sm.pack_rg, this.other_rg+sm.other_rg)

  def getAsVector =
    Vectors.dense(this.free_rg, this.payasgo_rg, this.pack_rg, this.other_rg)

}


object SubsModelData {
  def create(res: Result) = {
    val msisdn = Bytes.toString(res.getRow()).split("/")(0)
    val cells = res.listCells().
      map( v =>
        (Bytes.toString(v.getQualifierArray, v.getQualifierOffset, v.getQualifierLength),
          Bytes.toDouble(v.getValueArray, v.getValueOffset)))
    val free_rg = cells.filter( _._1.matches("^(100|101)/.*")).map( _._2).fold(0.0)(_ + _)
    val payasgo_rg = cells.filter( _._1.matches("^(102|104|105)/.*")).map( _._2).fold(0.0)(_ + _)
    val pack_rg = cells.filter( _._1.split("/")(0).toInt >= 120).map( _._2).fold(0.0)(_ + _)
    val other_rg = cells.filter( v => v._1.split("/")(0).toInt < 120 &&
                                      !v._1.matches("^(100|101)/.*") &&
                                      !v._1.matches("^(102|104|105)/.*")).map( _._2).fold(0.0)(_ + _)
    new SubsModelData(msisdn, free_rg, payasgo_rg, pack_rg, other_rg)
  }

}


