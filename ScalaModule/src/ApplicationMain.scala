import com.azercell.subscribermining.store.SubscriberQuery
import org.apache.hadoop.hbase.client.Result
import org.apache.hadoop.hbase.io.ImmutableBytesWritable
import org.apache.hadoop.hbase.mapreduce.TableInputFormat
import org.apache.spark.mllib.clustering.KMeans
import org.apache.spark.{SparkContext, SparkConf}
import sm.SubsModelData

/**
  * Created by rahimahmedov on 12/1/15.
  */




object ApplicationMain {

  def main (args: Array[String]) {


    val conf = new SparkConf().setAppName("Kluster")
    val sc = new SparkContext(conf)
    val hconf  = new SubscriberQuery("cdh03,cdh04,cdh05", 2181).from("subscriber").columns("DATA:/MB").getAsConfig()
    val indata = sc.newAPIHadoopRDD(hconf, classOf[TableInputFormat], classOf[ImmutableBytesWritable], classOf[Result]).map(v => {val sd = SubsModelData.create(v._2); (sd.msisdn, sd) }).reduceByKey( _ + _ )
    val labelAndData = indata.map( a => (a._2.msisdn, a._2.getAsVector) ).cache()
    val data = labelAndData.values.cache
    val model = new KMeans().setK(10).run(data)
    val clusterLabelCnt = sc.parallelize(labelAndData.collect().map( a =>  {val cluster = model.predict(a._2); (cluster, a._1)} )).countByKey



  }

}
