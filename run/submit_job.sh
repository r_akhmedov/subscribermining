#!/usr/bin/env bash
export SPARK_DIST_CLASSPATH=$(hadoop classpath)$(hbase classpath)
export HADOOP_CONF_DIR=/etc/hadoop/conf.cloudera.yarn/
spark-submit --master yarn-cluster \
    --driver-class-path /opt/cloudera/parcels/CDH/lib/hbase/lib/htrace-core-3.1.0-incubating.jar \
    --num-executors 4 \
    --executor-cores 2 \
    --executor-memory=8g \
    --driver-memory=8g \
    --class com.azercell.subscribermining.streaming.StreamMain SubscriberMining.jar hdfs://cdh01.azercell.com:8020/user/rahim/sm_config.xml